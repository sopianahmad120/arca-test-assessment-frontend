import { createRouter, createWebHistory } from 'vue-router'
import LoginView from '@/views/LoginView.vue'
import { useUserStore } from '@/stores/useUserStore.js'
import { storeToRefs } from 'pinia'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: LoginView
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/bonus/add',
      name: 'addBonus',
      component: () => import('../views/AddBonusView.vue'),
      meta: { requiresAuth: true }
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue'),
    }
  ]
})

router.beforeEach((to, from, next) => {
  const { isLoggedIn } = storeToRefs(useUserStore())

  if (to.matched.some((record) => record.meta.requiresAuth) && !isLoggedIn.value) {
    next({ name: 'login' })
  } else if ((to.path === '/login' || to.path === '/register') && isLoggedIn.value) {
    next('/')
  } else {
    next()
  }
})

export default router
