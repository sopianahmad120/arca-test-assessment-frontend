import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import api from '@/api/index.js'

export const useUserStore = defineStore('user', () => {
  const user = localStorage.getItem('user')
  const userInfo = ref(user ? JSON.parse(user) : null)

  const isLoggedIn = computed(() => !!userInfo.value)

  const updateUserInfo = (userData) => {
    if (userData) {
      console.log(userData)
      localStorage.setItem('user', JSON.stringify(userData))
      userInfo.value = userData
    } else {
      localStorage.removeItem('user')
      userInfo.value = null
    }
  }

  const handleAuth = async (type, formStore) => {
    await api.csrfCookie()
    const { data } = type === 'login' ? await api.login(formStore) : await api.register(formStore);
    updateUserInfo(data)
  }

  const signOut = async () => {
    await api.logout()
    updateUserInfo(null)
  }

  return { userInfo, isLoggedIn, handleAuth, signOut }
})
