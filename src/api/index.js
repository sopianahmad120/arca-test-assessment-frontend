import apiClient from '@/utils/axios'

function login(params) {
  return apiClient({
    url: 'api/login',
    method: 'post',
    data: params
  })
}

function register(params) {
  return apiClient({
    url: 'api/register',
    method: 'post',
    data: params
  })
}

function logout() {
  return apiClient({
    url: 'api/logout',
    method: 'post'
  })
}

function csrfCookie() {
  return apiClient({
    url: 'sanctum/csrf-cookie',
    method: 'get'
  })
}

export default {
  login,
  register,
  csrfCookie,
  logout
}
